﻿foreach (var line in File.ReadLines(args[0]))
{
    var startOfStream = FindStartOfStream(line);
    var startOfMessage = FindStartOfMessage(line);
    Console.WriteLine("Start of stream: {0}", startOfStream);
    Console.WriteLine("Start of message: {0}", startOfMessage);
}

int FindStartOfStream(string line) => FindStartMarker(line, 4);
int FindStartOfMessage(string line) => FindStartMarker(line, 14);

int FindStartMarker(string line, int size)
{
    var start = size - 1;
    for (var i = start; i < line.Length; i++) {
        if (new HashSet<char>(line.Substring(i - start, size)).Count == size)
            return i + 1;
    }
    return -1;
}

