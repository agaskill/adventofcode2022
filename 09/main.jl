module Aoc2209

struct Pos
    x::Int
    y::Int
end

struct Rope
    head::Pos
    tail::Pos
end

abstract type Direction end
struct Down <: Direction end
struct Up <: Direction end
struct Left <: Direction end
struct Right <: Direction end

function down(pos::Pos)
    Pos(pos.x, pos.y - 1)
end

function up(pos::Pos)
    Pos(pos.x, pos.y + 1)
end

function left(pos::Pos)
    Pos(pos.x - 1, pos.y)
end

function right(pos::Pos)
    Pos(pos.x + 1, pos.y)
end

function move(rope::Rope, ::Down)
    head = down(rope.head)
    tail = rope.tail
    if head.y < tail.y - 1
        tail = down(tail)
        if head.x > tail.x
            tail = right(tail)
        elseif head.x < tail.x
            tail = left(tail)
        end
    end
    Rope(head,tail)
end

function move(rope::Rope, ::Up)
    head = up(rope.head)
    tail = rope.tail
    if head.y > tail.y + 1
        tail = up(tail)
        if head.x > tail.x
            tail = right(tail)
        elseif head.x < tail.x
            tail = left(tail)
        end
    end
    Rope(head,tail)
end

function move(rope::Rope, ::Right)
    head = right(rope.head)
    tail = rope.tail
    if head.x > tail.x + 1
        tail = right(tail)
        if head.y > tail.y
            tail = up(tail)
        elseif head.y < tail.y
            tail = down(tail)
        end
    end
    Rope(head,tail)
end

function move(rope::Rope, ::Left)
    head = left(rope.head)
    tail = rope.tail
    if head.x < tail.x - 1
        tail = left(tail)
        if head.y > tail.y
            tail = up(tail)
        elseif head.y < tail.y
            tail = down(tail)
        end
    end
    Rope(head,tail)
end

function parse_direction(c::Char)::Direction
    if c == 'U'
        Up()
    elseif c == 'D'
        Down()
    elseif c == 'L'
        Left()
    elseif c == 'R'
        Right()
    else
        throw(ErrorException("Unsupported direction"))
    end
end

struct Move
    dir::Direction
    count::Int
end

function parseline(line)
    dir = line[1]
    count = line[3:end]
    Move(parse_direction(dir), parse(Int, count))
end

function readfile(filename)
    map(parseline, eachline(filename))
end

function part1(filename)
    moves = readfile(filename)
    rope = Rope(Pos(0,0), Pos(0,0))
    tails = Set([rope.tail])
    for m in moves
        for i in 1:m.count
            rope = move(rope, m.dir)
            push!(tails, rope.tail)
        end
    end
    tails
end

end