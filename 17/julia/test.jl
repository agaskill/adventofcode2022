include("tmp.jl")
import .Tmp

start_time = time()
Tmp.run_file("example.txt", 2022)
println("Took $(time() - start_time) seconds")
Tmp.run_file("input.txt", 2022)
println("Took $(time() - start_time) seconds")
Tmp.run_file("example.txt", 1000000000000)
println("Took $(time() - start_time) seconds")
Tmp.run_file("input.txt", 1000000000000)
println("Took $(time() - start_time) seconds")