abstract type FsNode end

struct FsDirectory <: FsNode
    name::String
    contents::Vector{FsNode}
end

struct FsFile <: FsNode
    name::String
    size::Int
end

function fssize(f::FsFile)
    f.size
end

function fssize(d::FsDirectory)
    if isempty(d.contents)
        0
    else
        sum(fssize, d.contents)
    end
end

function parseFsNode(line::String)
    spacepos = findfirst(' ', line)
    sizepart = line[1:spacepos-1]
    namepart = line[spacepos+1:end]
    if sizepart == "dir"
        FsDirectory(namepart, [])
    else
        FsFile(namepart, parse(Int, sizepart))
    end
end

function printfs(node::FsDirectory, depth)
    println(repeat("  ", depth), "- ", node.name, " (dir)")
    for n in node.contents
        printfs(n, depth + 1)
    end
end

function printfs(node::FsFile, depth)
    println(repeat("  ", depth), "- ", node.name, " (file, size=", node.size, ")")
end

function readinput(file)
    root = FsDirectory("/", [])
    currdir = root
    parents::Vector{FsDirectory} = []
    listing = false
    
    for line in eachline(file)
        if line == "\$ ls"
            println("Listing contents")
            listing = true
        elseif line == "\$ cd /"
            println("Moving to root")
            currdir = root
        elseif line == "\$ cd .."
            println("Moving up one level")
            currdir = pop!(parents)
        elseif startswith(line, "\$ cd ")
            push!(parents, currdir)
            let requested = line[6:end]
                println("Moving in to $requested")
                currdir = currdir.contents[findfirst(x -> x.name == requested, currdir.contents)]
            end
        elseif startswith(line, "\$")
            throw(ArgumentError("Unknown command"))
        elseif !listing
            throw(ArgumentError("Unexpected non-command"))
        else
            println("Adding directory entry for $line")
            push!(currdir.contents, parseFsNode(line))
        end
    end

    printfs(root, 0)

    root
end

function sumAll(d::FsDirectory)
    size = fssize(d)
    if (size > 100000)
        size = 0
    end
    for entry in d.contents
        if isa(entry, FsDirectory)
            size += sumAll(entry)
        end
    end
    size
end

function findSmallestDirectoryBiggerThan(d::FsDirectory, needed, bestSize)
    size = fssize(d)
    if size >= needed
        # if it's smaller than the amount needed, there's no use considering it

        # update the bestSize for all the subdirectories
        for entry in d.contents
            if isa(entry, FsDirectory)
                bestSize = min(bestSize, findSmallestDirectoryBiggerThan(entry, needed, bestSize))
            end
        end

        # then see if this one is itself a better candidate, in case none of the subdirectories were good candidate
        bestSize = min(bestSize, size)
    end
    bestSize
end

root = readinput(ARGS[1])

totalsize = fssize(root)
println("Size of root: $totalsize")
freespace = 70000000 - totalsize
needed = 30000000 - freespace
println("Need to delete $needed")

# println(sumAll(root))
bestToDelete = findSmallestDirectoryBiggerThan(root, needed, 70000000)
println("Can delete directory of size $bestToDelete to free up space")