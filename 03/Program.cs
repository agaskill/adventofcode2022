﻿var prioritySum = 0;
var seq = 0;
var groupPrioritySum = 0;
HashSet<char> groupSet = new HashSet<char>();
foreach (var line in File.ReadLines(args[0]))
{
    var contents = line.Trim();
    var c1 = contents.Substring(0, contents.Length / 2);
    var c2 = contents.Substring(contents.Length / 2);
    var common = c1.Intersect(c2).Single();
    var commonValue = Priority(common);
    Console.WriteLine("common element: {0}", common);
    Console.WriteLine("Prioity value is {0}", commonValue);
    prioritySum += commonValue;

    if (seq == 0) {
        groupSet.Clear();
        groupSet.UnionWith(contents);
    }
    else
    {
        groupSet.IntersectWith(contents);
    }

    seq = (seq + 1) % 3;
    if (seq == 0)
    {
        var groupCommon = groupSet.Single();
        var groupValue = Priority(groupCommon);
        Console.WriteLine("Common element in group is {0}", groupCommon);
        Console.WriteLine("Group priority value is {0}", groupValue);
        groupPrioritySum += groupValue;
    }
}
Console.WriteLine("Priority sum: {0}", prioritySum);
Console.Write("Group priority sum: {0}", groupPrioritySum);

static int Priority(char c)
{
    if (c >= 'a')
        return 1 + c - 'a';
    return 27 + c - 'A';
}