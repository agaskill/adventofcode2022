﻿public static class Program
{
    internal class Line
    {
        private readonly List<Point> points;

        public IEnumerable<Point> Points => points;

        private Line(List<Point> points)
        {
            this.points = points;
        }

        public static Line Parse(string line)
        {
            var pointStrings = line.Split(" -> ");
            return new Line(pointStrings.Select(ParsePoint).ToList());
        }

        private static Point ParsePoint(string str)
        {
            var parts = str.Split(',');
            return new Point(int.Parse(parts[0]), int.Parse(parts[1]));
        }

        public override string ToString()
        {
            return string.Join(" -> ", points.Select(p => $"{p.x},{p.y}"));
        }

        public int MinX => points.Min(p => p.x);
        public int MaxX => points.Max(p => p.x);
        public int MaxY => points.Max(p => p.y);
    }

    internal record Point(int x, int y);

    internal class Grid
    {
        private char[] state = Array.Empty<char>();
        private int xoffset;
        private int width;
        private int height;

        public void Build(IEnumerable<Line> lines)
        {
            var minx = lines.Min(l => l.MinX) - 2;
            var maxx = lines.Max(l => l.MaxX) + 1;
            var maxy = lines.Max(l => l.MaxY) + 1;
            xoffset = minx;
            width = maxx + 1 - minx;
            height = maxy + 1;
            state = new char[width * height];
            Array.Fill(state, '.');
            foreach (var line in lines)
            {
                Draw(line);
            }
        }

        private void Draw(Line line)
        {
            Point? prev = null;
            foreach (var point in line.Points)
            {
                if (prev == null)
                {
                    prev = point;
                }
                else
                {
                    DrawFrom(prev, point);
                    prev = point;
                }
            }
        }

        private void DrawFrom(Point a, Point b)
        {
            if (a.x == b.x)
            {
                var y1 = a.y;
                var y2 = b.y;
                if (y2 < y1)
                {
                    y1 = b.y;
                    y2 = a.y;
                }
                for (int i = y1; i <= y2; i++) {
                    MarkStone(a.x, i);
                }
            }
            else if (a.y == b.y)
            {
                var x1 = a.x;
                var x2 = b.x;
                if (x2 < x1)
                {
                    x1 = b.x;
                    x2 = a.x;
                }
                for (int i = x1; i <= x2; i++) {
                    MarkStone(i, a.y);
                }
            }
            else
            {
                throw new InvalidOperationException("Either Xs or Ys have to match");
            }
        }

        private void MarkStone(int x, int y)
        {
            state[IndexFor(x, y)] = '#';
        }

        private void MarkSand(int x, int y)
        {
            state[IndexFor(x, y)] = 'o';
        }

        private int IndexFor(int x, int y) => (y * width) + (x - xoffset);

        public int TotalAdded {get; private set;}

        public bool AddSand()
        {
            int x = 500;
            int y = 0;
            if (state[IndexFor(x, y)] == 'o')
                return false;
            
            TotalAdded++;
            do {
                if (y == (height - 1))
                {
                    // hits ground
                    MarkSand(x, y);
                    return true;
                }
                if (state[IndexFor(x, y + 1)] == '.')
                {
                    y++;
                }
                else if (x == xoffset)
                {
                    // sand would also be added to the left to support this one
                    TotalAdded += (height - 1) - y;
                    MarkSand(x, y);
                    return true;
                }
                else if (state[IndexFor(x - 1, y + 1)] == '.')
                {
                    x--;
                    y++;
                }
                else if (x == xoffset + width - 1)
                {
                    // sand would also be added to the right to support this one
                    TotalAdded += (height - 1) - y;
                    MarkSand(x, y);
                    return true;                }
                else if (state[IndexFor(x + 1, y + 1)] == '.')
                {
                    x++;
                    y++;
                }
                else
                {
                    // nowhere else to go, sand stays here
                    MarkSand(x, y);
                    return true;
                }
            } while (true);
        }

        public override string ToString()
        {
            var str = "";
            for (int i = 0; i < height; i++)
            {
                str += new string(state, i * width, width) + "\n";
            }
            return str;
        }
    }

    public static void Main(string[] args)
    {
        var input = File.ReadLines(args[0]);
        var lines = input.Select(Line.Parse).ToList();
        var grid = new Grid();
        grid.Build(lines);
        Console.WriteLine(grid);

        while (grid.AddSand())
        {
            //Console.WriteLine(grid);
            //Console.ReadLine();
        }
        Console.WriteLine(grid);
        Console.WriteLine("Added {0} blocks of sand", grid.TotalAdded);
    }
}