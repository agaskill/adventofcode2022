﻿var elves = new List<int>() { 0 };

foreach (var line in File.ReadLines(args[0]))
{
    if (string.IsNullOrWhiteSpace(line))
    {
        elves.Add(0);
    }
    else
    {
        elves[elves.Count - 1] += int.Parse(line);
    }
}

elves.Sort();
elves.Reverse();

Console.WriteLine("Max are:");
foreach (var top in elves.Take(3))
{
    Console.WriteLine(top);
}
Console.WriteLine("Which together is {0}", elves.Take(3).Sum());