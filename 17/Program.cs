﻿using System.Diagnostics;

var jetstream = File.ReadAllLines(args[0])[0];
Console.WriteLine("jetstream pattern is {0}", jetstream.Length);

var sw = new Stopwatch();
var sim = new Simulation(jetstream);
sw.Start();
sim.Run();
Console.WriteLine(sw.Elapsed);

if (Console.ReadLine()?.Trim() == "y")
    sim.Print();

class Simulation
{
    public Simulation(string jetstream)
    {
        this.jetstream = jetstream;
    }
    private readonly string jetstream;
    private readonly List<string> grid = new();
    private int fallen;
    private int jetIndex;
    private Shape current = Shape.Shapes[0];
    private int currentX = 2;
    private int currentY = 3;

    public void Run()
    {
        while (fallen < 8) {
            ApplyJetstream();
            if (!FallOneUnit()) {
                FixInPlace();
                Print();
                fallen++;
                current = Shape.Shapes[fallen % Shape.Shapes.Length];
                currentX = 2;
                currentY = grid.Count + 3;
            }
        }

        Console.WriteLine("Height is {0}", grid.Count);
    }

    public void Print() {
        var i = grid.Count;
        while (i-- > 0) {
            Console.WriteLine("{0:0000} {1}", i, grid[i]);
        }
        Console.WriteLine("-------");
    }

    private void ApplyJetstream() {
        var dir = jetstream[jetIndex++];
        if (jetIndex == jetstream.Length)
            jetIndex = 0;
        if (dir == '<') TryMoveLeft();
        else if (dir == '>') TryMoveRight();
        else throw new InvalidOperationException("Invalid direction");
    }

    private void TryMoveLeft() {
        if (TryDrawAt(currentX - 1, currentY)) {
            currentX--;
        }
    }

    private void TryMoveRight() {
        if (TryDrawAt(currentX + 1, currentY)) {
            currentX++;
        }
    }

    private bool FallOneUnit() {
        if (TryDrawAt(currentX, currentY - 1)) {
            currentY--;
            return true;
        }
        return false;
    }

    private bool TryDrawAt(int xoffset, int yoffset)
    {
        // try to draw the rock at the new location.  If it intersects any
        // existing rocks, it's not valid

        for (int x = 0; x < current.width; x++) {
            var gridx = xoffset + x;

            // can't go outside the bounds
            if (gridx < 0 || gridx >= 7) return false;

            for (int y = 0; y < current.height; y++) {
                // if the rock is empty at this point, it's okay
                // if (current.pattern[y][x] == ' ') continue;

                var gridy = yoffset + y;

                // can't draw below the floor
                if (gridy < 0) return false;

                // if the rock is above the highest point, it's okay
                if (gridy >= grid.Count) continue;

                // can't draw rock over rock
                if (grid[gridy][gridx] == '#' && current.pattern[y][x] == '#') return false;
            }
        }

        // check passes
        return true;
    }

    private void FixInPlace()
    {
        for (int y = 0; y < current.height; y++) {
            var gridy = currentY + y;
            string prev;
            if (gridy == grid.Count) {
                prev = "       ";
                grid.Add(prev);
            } else {
                prev = grid[gridy];
            }
            grid[gridy] = prev.Substring(0, currentX)
                + current.pattern[y]
                + prev.Substring(currentX + current.width, 7 - (currentX + current.width));
        }
    }
}

record Shape(int width, int height, string[] pattern)
{
    public static readonly Shape[] Shapes = new[] {
        new Shape(4, 1, new[] { "####" }),
        new Shape(3, 3, new[] { " # ", "###", " # " }),
        new Shape(3, 3, new[] { "###", "  #", "  #" }),
        new Shape(1, 4, new[] { "#", "#", "#", "#" }),
        new Shape(2, 2, new[] { "##", "##" }),
    };
}