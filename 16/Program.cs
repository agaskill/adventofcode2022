﻿using System.Collections.Immutable;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;

static class Program
{
    static int considered;
    static int eliminated;
    static int duplicate;
    static Stopwatch stopwatch = new Stopwatch();
    internal static int totalMinutes;
    internal static int totalCreatures;

    public static void Main(string[] args)
    {
        var inputFile = args[0];

        var valves = File.ReadLines(inputFile).Select(Valve.Parse).ToList();
        foreach (var valve in valves) {
            //Console.WriteLine(valve);
            var map = valves.ToDictionary(v => v.id);
            valve.tunnels.Sort((a, b) => map[b].flowRate.CompareTo(map[a].flowRate));
        }

        if (args[1] == "part1") {
            totalMinutes = 30;
            totalCreatures = 1;
        } else if (args[1] == "part2") {
            totalMinutes = 26;
            totalCreatures = 2;
        } else {
            totalMinutes = int.Parse(args[1]);
            totalCreatures = int.Parse(args[2]);
        }

        var locs = Enumerable.Repeat("AA", totalCreatures).ToImmutableList();
        var paths = Enumerable.Repeat(new Path("AA", null), totalCreatures).ToImmutableList();
        stopwatch.Start();
        var initialState = new SystemState(locs, paths, valves.ToImmutableDictionary(v => v.id), 0, 0, 0);
        var horizon = new PriorityQueue<SystemState, int>();
        var seen = new HashSet<string>();
        SystemState? best = null;
        horizon.Enqueue(initialState, 0);
        while (horizon.TryDequeue(out var current, out var potential))
        {
            considered++;
            if (stopwatch.ElapsedMilliseconds > 10000) {
                stopwatch.Restart();
                Console.WriteLine("--------------------");
                Console.WriteLine("Total states considered: {0}; eliminated: {1}; duplicates: {2}, horizon: {3}", considered, eliminated, duplicate, horizon.Count);
            }

            // if potential is less then best, skip it
            if (best != null && potential <= best.pressureReleased) continue;

            if (current.valves.Values.All(v => v.flowRate == 0 || v.open)) {
                // all valves open, nothing left to do but let pressure release
                current = current.LetTimeRunOut();
            }
            if (current.elapsed == totalMinutes) {
                if (best == null || current.pressureReleased > best.pressureReleased)
                {
                    Console.WriteLine("*******************");
                    Console.WriteLine("New best {0}", current);
                    best = current;
                }
            }
            else {
                foreach (var next in current.Expand()) {
                    // Don't backtrack (same positions as a previous state with the same flow rate)
                    if (seen.Contains(next.StateCode())) {
                        duplicate++;
                    }
                    // if it has the same or less potential than the current best, skip
                    else if (best != null && next.Potential < best.pressureReleased) {
                        eliminated++;
                    }
                    else {
                        seen.Add(next.StateCode());
                        horizon.Enqueue(next, int.MaxValue - next.Potential);
                    }
                }
            }
        }
        Console.WriteLine("====================");
        Console.WriteLine(best);
        Console.WriteLine("Total states considered: {0}; eliminated: {1}; duplicates: {2}", considered, eliminated, duplicate);
    }
}

record SystemState(
    ImmutableList<string> locations,
    ImmutableList<Path> paths,
    ImmutableDictionary<string, Valve> valves,
    int totalFlowRate,
    int pressureReleased,
    int elapsed)
{
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Elapsed: {elapsed}; pressure released: {pressureReleased}; total flow rate: {totalFlowRate}");
        foreach (var path in paths) {
            sb.Append("Path: ").AppendLine(path?.ToString());
        }
        //+ "\nValves: " + string.Join(", ", valves.Values.Select(v => $"{v.id}: {v.State}"));
        return sb.ToString();
    }

    private int _potential = -1;

    public int Potential
    {
        get {
            if (_potential < 0)
            {
                var potential = pressureReleased + totalFlowRate * (Program.totalMinutes - elapsed);

                var currValves = locations.Select(l => valves[l]).ToList();
                // could potentially open any closed valve by getting there
                foreach (var valve in valves.Values.Where(v => v.flowRate > 0 && !v.open))
                {
                    int dist;
                    if (locations.Contains(valve.id)) {
                        dist = 0;
                    } else if (currValves.Any(v => v.tunnels.Contains(valve.id))) {
                        dist = 1;
                    } else {
                        // optimistic
                        dist = 2;
                    }
                    // most you can get from it is the remaining minutes times the time it takes to get there and open it
                    var mins = Program.totalMinutes - (elapsed + dist + 1);
                    if (mins > 0) {
                        potential += valve.flowRate * mins;
                    }
                }
                _potential = potential;
            }

            return _potential;
        }
    }

    public SystemState LetTimeRunOut()
    {
        return new SystemState(
            locations,
            paths,
            valves,
            totalFlowRate,
            pressureReleased + totalFlowRate * (Program.totalMinutes - elapsed),
            Program.totalMinutes);
    }

    private string? _stateCode;

    public string StateCode()
    {
        if (_stateCode == null) {
            _stateCode = string.Join(" ", locations.Sort())
                + totalFlowRate.ToString(" 000");
        }
        return _stateCode;
    }

    record PartialState(string location, Path path, Valve? valve);

    private List<PartialState> ExpandPartial(string location, Path path)
    {
        var options = new List<PartialState>();
        var curr = valves[location];
        
        // Could stay and open the valve, if it is closed and the flow rate is nonzero
        if (!curr.open && curr.flowRate > 0) {
            options.Add(new PartialState(location, new Path("op", path), curr with { open = true}));
        }

        // Could follow any path (except back where you came)
        foreach (var tunnel in curr.tunnels) {
            if (path.action != "op" &&  path.prev?.action == tunnel) continue;
            options.Add(new PartialState(tunnel, new Path(tunnel, path), null));
        }

        return options;
    }

    private ImmutableDictionary<string, Valve> WithValves(IEnumerable<Valve> openedValves)
    {
        var result = valves;
        foreach (var valve in openedValves) {
            result = result.SetItem(valve.id, valve);
        }
        return result;
    }

    IEnumerable<ImmutableList<PartialState>> ExpandPartials(int idx, ImmutableList<PartialState> accumulated)
    {
        if (idx == locations.Count)
        {
            yield return accumulated;
        }
        else
        {
            foreach (var state in ExpandPartial(locations[idx], paths[idx]))
            {
                foreach (var expanded in ExpandPartials(idx + 1, accumulated.Add(state)))
                {
                    yield return expanded;
                }
            }
        }
    }

    public IEnumerable<SystemState> Expand()
    {
        foreach (var combo in ExpandPartials(0, ImmutableList<PartialState>.Empty))
        {
            var openedValves = combo.Where(c => c.valve != null).Select(c => c.valve!).ToList();
            // make sure all opened valves are distinct
            if (openedValves.DistinctBy(x => x.id).Count() != openedValves.Count) continue;
            yield return new SystemState(
                combo.Select(c => c.location).ToImmutableList(),
                combo.Select(c => c.path).ToImmutableList(),
                WithValves(openedValves),
                totalFlowRate + openedValves.Sum(v => v.flowRate),
                pressureReleased + totalFlowRate,
                elapsed + 1);
        }
    }
}

record Path(string action, Path? prev)
{
    public override string ToString()
    {
        return prev == null ? action : (prev.ToString() + ", " + action);
    }
}

record Valve(string id, int flowRate, bool open, List<string> tunnels)
{
    public static Valve Parse(string line)
    {
        var match = Regex.Match(line, @"Valve ([A-Z]+) has flow rate=(\d+); tunnels? leads? to valves? ([A-Z]+, )*([A-Z]+)");
        if (match.Success) {
            var id = match.Groups[1].Value;
            var flowRate = int.Parse(match.Groups[2].Value);
            var tunnels = new List<string>();
            if (match.Groups[3].Success) {
                tunnels.AddRange(match.Groups[3].Captures.Select(x => x.Value.Substring(0, x.Length - 2)));
            }
            tunnels.Add(match.Groups[4].Value);
            return new Valve(id, flowRate, false, tunnels);
        }
        throw new InvalidDataException("No match to line " + line);
    }

    public string State => open ? "open" : "closed";

    public override string ToString()
    {
        return $"new Valve({id}, {flowRate}, {open}, new[] {{ {string.Join(", ", tunnels)} }})";
    }
}