﻿var fullyContained = 0;
var overlaps = 0;
foreach (var line in File.ReadLines(args[0]))
{
    var assignments = line.Trim().Split(',');
    var range1 = Range.Parse(assignments[0]);
    var range2 = Range.Parse(assignments[1]);
    if (range1.FullyContains(range2) ||
        range2.FullyContains(range1))
    {
        fullyContained++;
    }
    else if (range1.Overlaps(range2))
    {
        overlaps++;
    }
}

Console.WriteLine("Assignments in which one range fully contains the other: {0}", fullyContained);
Console.WriteLine("Assignments with any overlap: {0}", fullyContained + overlaps);

record Range(int From, int To)
{
    public static Range Parse(string str)
    {
        var range = str.Split('-');
        var from = int.Parse(range[0]);
        var to = int.Parse(range[1]);
        if (from > to) throw new InvalidDataException("From must not be above To");
        return new Range(from , to);
    }

    public bool FullyContains(Range other) => other.From >= From && other.To <= To;

    public bool Overlaps(Range other) => other.To >= From && other.From <= To;
}