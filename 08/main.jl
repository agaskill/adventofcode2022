module AOC2208

function parsefile(filename)
    stack(
        map(
            line -> map(
                n -> parse(Int, n),
                split(line, "")),
            eachline(filename)),
        dims=1)
end
    
function mapvisible(trees)
    rows, cols = size(trees)
    visible = falses(rows, cols)
    # left and right
    for row in 1:rows
        # from left
        max = trees[row,1]
        visible[row,1] = true
        for col in 2:cols
            if trees[row,col] > max
                visible[row,col] = true
                max = trees[row,col]
            end
        end

        # from right
        max = trees[row,cols]
        visible[row,cols] = true
        for col in cols-1:-1:1
            if trees[row,col] > max
                visible[row,col] = true
                max = trees[row,col]
            end
        end
    end

    # up and down
    for col in 1:cols
        # from top
        max = trees[1,col]
        visible[1,col] = true
        for row in 2:rows
            if trees[row,col] > max
                visible[row,col] = true
                max = trees[row,col]
            end
        end

        # from bottom
        max = trees[rows,col]
        visible[rows,col] = true
        for row in rows-1:-1:1
            if trees[row,col] > max
                visible[row,col] = true
                max = trees[row,col]
            end
        end
    end

    visible
end

function countvisible(visible)
    length(filter(identity, visible))
end

function process_part1(filename)
    countvisible(mapvisible(parsefile("input.txt")))
end

function process_part2(filename)
    trees = parsefile(filename)
    rows, cols = size(trees)
    scores = zeros(Int, (rows, cols))
    for row in 1:rows
        for col in 1:cols
            scores[row,col] = calculate_score(trees, row, col)
        end
    end
    maximum(scores)
end

function calculate_score(trees, row, col)
    height = trees[row,col]
    find_same_or_higher_left(trees, height, row, col) *
    find_same_or_higher_right(trees, height, row, col) *
    find_same_or_higher_up(trees, height, row, col) *
    find_same_or_higher_down(trees, height, row, col)
end

function find_same_or_higher_left(trees, height, row, col)
    for c in col-1:-1:1
        if trees[row,c] >= height
            return col - c
        end
    end
    return col - 1
end

function find_same_or_higher_right(trees, height, row, col)
    cols = size(trees, 2)
    for c in col+1:cols
        if trees[row,c] >= height
            return c - col
        end
    end
    return cols - col
end

function find_same_or_higher_up(trees, height, row, col)
    for r in row-1:-1:1
        if trees[r,col] >= height
            return row - r
        end
    end
    return row - 1
end

function find_same_or_higher_down(trees, height, row, col)
    rows = size(trees, 1)
    for r in row+1:rows
        if trees[r,col] >= height
            return r - row
        end
    end
    return rows - row
end


#let trees = parsefile(ARGS[1])
#@show mapvisible(trees)

end