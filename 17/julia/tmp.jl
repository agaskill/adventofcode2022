module Tmp
export run_file

shapes = [
    trues(1, 4),
    BitMatrix([
        0 1 0;
        1 1 1;
        0 1 0;
    ]),
    BitMatrix([
        1 1 1;
        0 0 1;
        0 0 1;
    ]),
    trues(4, 1),
    BitMatrix([
        1 1;
        1 1;
    ])
]

function run_file(filename, nblocks)
    line = open(readline, filename, "r")
    jets = [if c == '>' 1 elseif c == '<' -1 else throw("invalid char") end for c in line]
    blockCount = 0
    jetIndex = 0
    jetCount = length(line)
    cave = falses(10000, 7)
    totalHeight = 0
    base = 0
    while blockCount < nblocks
        block = shapes[1 + blockCount % 5]
        blockCount += 1
        xOffset = 2
        yOffset = totalHeight + 3
        falling = true
        while falling
            direction = jets[1 + jetIndex % jetCount]
            jetIndex += 1
            nextX = xOffset + direction
            if no_conflicts(cave, block, nextX, yOffset)
                xOffset = nextX
            end
            nextY = yOffset - 1
            if no_conflicts(cave, block, xOffset, nextY)
                yOffset = nextY
            else
                falling = false
                cave = add_block(cave, block, xOffset, yOffset)
            end
        end
        #display(cave)
    end
    println("After $blockCount blocks, height is $(size(cave, 1))")
end

function no_conflicts(cave, block, xOffset, yOffset)
    (caveHeight, caveWidth) = size(cave)
    (blockHeight, blockWidth) = size(block)
    if yOffset < 0 || xOffset < 0 || xOffset + blockWidth > caveWidth
        return false
    end
    for y in 1:blockHeight
        for x in 1:blockWidth
            if yOffset + y <= caveHeight && block[y, x] && cave[yOffset + y, xOffset + x]
                return false
            end
        end
    end
    true
end

function add_block(cave, block, xOffset, yOffset)
    caveHeight = size(cave, 1)
    (blockHeight, blockWidth) = size(block)
    addedHeight = yOffset + blockHeight - caveHeight
    if addedHeight > 0
        cave = [cave; falses(addedHeight, 7)]
    end
    for y in 1:blockHeight
        for x in 1:blockWidth
            if block[y, x]
                cave[yOffset + y, xOffset + x] = true
            end
        end
    end
    cave
end

end