with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Containers.Vectors;

procedure Main is

   type Direction is (Left, Right);

   type DirectionSequence is array (Integer range <>) of Direction;

   type SquareState is (O, X);

   type ShapeDef is array (0..3, 0..3) of SquareState;

   type RowState is array (0..6) of SquareState;

   type ShapeIndex is mod 5;

   package RowVectors is new
      Ada.Containers.Vectors (
         Index_Type => Natural,
         Element_Type => RowState);

   cave : RowVectors.Vector;

   function Load_Data (fileName: String) return DirectionSequence is
   file : File_Type;
   begin
      Open(file, In_File, fileName);
      declare
         line: constant String := Get_Line(file);
         result: DirectionSequence (line'Range);
      begin
         Close(file);
         for i in line'Range loop
            case line(i) is
               when '<' => result(i) := Left;
               when '>' => result(i) := Right;
               when others => Put_Line ("unexpected");
            end case;
         end loop;
         return result;
      end;
   end Load_Data;

   shapes : constant array (ShapeIndex) of ShapeDef := (
      ((O,O,O,O),
       (O,O,O,O),
       (O,O,O,O),
       (X,X,X,X)),

      ((O,O,O,O),
       (O,X,O,O),
       (X,X,X,O),
       (O,X,O,O)),

      ((O,O,O,O),
       (O,O,X,O),
       (O,O,X,O),
       (X,X,X,O)),

      ((X,O,O,O),
       (X,O,O,O),
       (X,O,O,O),
       (X,O,O,O)),

      ((O,O,O,O),
       (O,O,O,O),
       (X,X,O,O),
       (X,X,O,O)));
   
   directions : constant DirectionSequence := Load_Data("example.txt");

begin
   for i in directions'Range loop
      Put_Line (Direction'Image (directions(i)));
   end loop;
end Main;
