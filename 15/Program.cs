﻿using System.Numerics;
using System.Text.RegularExpressions;

public static class Program
{
    public static void Main(string[] args)
    {
        var sensors = File.ReadLines(args[0])
            .Select(ParseLine)
            .ToList();

        foreach (var sensor in sensors)
        {
            Console.WriteLine(sensor);
        }

        var coords = FindBeacon(sensors, 0, int.Parse(args[1]));
        if (coords == null)
            Console.WriteLine("It can't be anywhere");
        else
            Console.WriteLine("It's at {0}, frequency {1}", coords, coords.x * 4000000L + coords.y);
    }

    static void GetExcludedLocationCount(IEnumerable<Sensor> sensors)
    {
        var locationsWithoutSensors = sensors
            .SelectMany(s => s.LocationsWithoutBeaconsInRow(2000000))
            .Distinct()
            .Count();

        Console.WriteLine("Locations in row 2000000 without sensors: {0}", locationsWithoutSensors);
    }

    static Point? FindBeacon(IEnumerable<Sensor> sensors, int min, int max)
    {
        foreach (var sensor in sensors)
        {
            Console.WriteLine("Checking around sensor {0}", sensor);
            var otherSensors = sensors.Where(s => !object.ReferenceEquals(sensor, s));
            foreach (var point in sensor.LocationsOutsideRange(min, max))
            {
                if (BeaconCanBeAt(otherSensors, point.x, point.y))
                {
                    return point;
                }
            }
        }
        // for (int x = min; x < max; x++)
        // {
        //     Console.WriteLine("Checking column {0}", x);
        //     for (int y = min; y < max; y++)
        //     {
        //         if (BeaconCanBeAt(sensors, x, y))
        //         {
        //             return new Point(x, y);
        //         }
        //     }
        // }
        return null;
    }

    static bool BeaconCanBeAt(IEnumerable<Sensor> sensors, int x, int y)
    {
        foreach (var sensor in sensors)
        {
            if (sensor.IsInRange(x, y))
                return false;
        }
        return true;
    }

    static Sensor ParseLine(string line)
    {
        var match = Regex.Match(line, @"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)");
        if (!match.Success)
            throw new InvalidOperationException("Invalid input: " + line);
        return new Sensor(
            location: new Point(
                int.Parse(match.Groups[1].Value),
                int.Parse(match.Groups[2].Value)),
            beacon: new Point(
                int.Parse(match.Groups[3].Value),
                int.Parse(match.Groups[4].Value)));
    }
}

record Point(int x, int y)
{
    public override string ToString()
    {
        return $"({x},{y})";
    }
}

record Sensor(Point location, Point beacon)
{
    public override string ToString()
    {
        return $"Location: {location}, Beacon: {beacon}";
    }

    public int DistanceToBeacon => DistanceTo(beacon.x, beacon.y);

    public int DistanceTo(int x, int y) => Math.Abs(location.x - x) + Math.Abs(location.y - y);

    public bool IsInRange(int x, int y)
    {
        return DistanceTo(x, y) <= DistanceToBeacon;
    }

    public IEnumerable<Point> LocationsWithoutBeaconsInRow(int row)
    {
        Console.WriteLine("Checking sensor {0}", this);
        var distanceToBeacon = DistanceToBeacon;
        Console.WriteLine("  distance to beacon is {0}, which means y span is {1} to {2}", distanceToBeacon, location.y - distanceToBeacon, location.y + distanceToBeacon);
        var xrange = distanceToBeacon - Math.Abs(location.y - row);
        if (xrange >= 0)
        {
            Console.WriteLine("  checking {0}*2 + 1 locations in row {1} from {2} to {3}", xrange, row, location.x - xrange, location.x + xrange);
            for (int x = location.x - xrange; x <= location.x + xrange; x++)
            {
                if (x != beacon.x || row != beacon.y)
                {
                    yield return new Point(x, row);
                }
            }
        }
        else
        {
            Console.WriteLine("  does not span row {0}", row);
        }
    }

    public IEnumerable<Point> LocationsOutsideRange(int min, int max)
    {
        var dist = DistanceToBeacon + 1;
        var minx = Math.Max(min, location.x - dist);
        var maxx = Math.Min(max, location.x + dist);
        for (var x = minx; x <= maxx; x++)
        {
            var yrange = dist - Math.Abs(location.x - x);
            var miny = location.y - yrange;
            var maxy = location.y + yrange;
            if (miny >= min && miny <= max)
                yield return new Point(x, miny);
            if (maxy >= min && maxy <= max)
                yield return new Point(x, maxy);
        }
    }
}