﻿using System.Text.RegularExpressions;

var root = new DirectoryEntry("", null);
var current = root;
var lines = File.ReadLines(args[0]);
var fs = ParseLines(lines);
fs.Print();
Console.WriteLine("Sum of directories under threshold: {0}", SumUnderThreshold(fs.Root, 100000));
var freeSpace = 70000000 - fs.Root.TotalSize();
var needed = 30000000 - freeSpace;
var toDelete = FindSmallestToDelete(fs.Root, needed);

DirectoryEntry FindSmallestToDelete(DirectoryEntry entry, int needed)
{
    var totalSize = entry.TotalSize();
    if (totalSize > needed) {
        // it could be this one, but see if it contains a smaller one
        foreach (var child in entry.Entries.OfType<DirectoryEntry>())
        {
            
        }
    }
}

FileSystem ParseLines(IEnumerable<string> lines)
{
    var fs = new FileSystem();
    var e = lines.GetEnumerator();
    if (e.MoveNext()) {
        ReadCommand(fs, e);
    }
    return fs;
}

void ReadCommand(FileSystem fs, IEnumerator<string> e)
{
    var line = e.Current;
    while (line != null)
    {
        Match match;
        
        match = Regex.Match(line, @"^\$ cd (\S+)$");
        if (match.Success) {
            var dir = match.Groups[1].Value;
            if (dir == "..")
                fs.MoveUp();
            else if (dir == "/")
                fs.MoveToRoot();
            else
                fs.MoveDown(dir);
            if (e.MoveNext())
            {
                line = e.Current;
                continue;
            }
            break;
        }

        match = Regex.Match(line, @"^\$ ls$");
        if (match.Success) {
            ReadEntries(fs, e);
            line = e.Current;
            continue;
        }

        throw new InvalidDataException("Unexpected line pattern " + line);
    }
}

void ReadEntries(FileSystem fs, IEnumerator<string> e)
{
    while (e.MoveNext())
    {
        var line = e.Current;
        if (line.StartsWith('$'))
            break;
        var parts = line.Split(' ');
        if (parts[0] == "dir")
            fs.AddDirectory(parts[1]);
        else
            fs.AddFile(parts[1], int.Parse(parts[0]));
    }
}

int SumUnderThreshold(DirectoryEntry dir, int threshold)
{
    var result = 0;
    foreach (var entry in dir.Entries.OfType<DirectoryEntry>())
    {
        result += SumUnderThreshold(entry, threshold);
    }
    var currentSize = dir.TotalSize();
    if (currentSize <= threshold)
    {
        result += currentSize;
    }
    return result;
}

class FileSystem
{
    public DirectoryEntry Root {get;}
    public DirectoryEntry Current {get; private set;}

    public FileSystem()
    {
        Root = new DirectoryEntry("/", null);
        Current = Root;
    }

    public void MoveToRoot() => Current = Root;
    public void MoveUp() => Current = Current.Parent ?? throw new InvalidOperationException("No parent");
    public void MoveDown(string directory) => Current = (DirectoryEntry)Current.Entries.First(e => e.Name == directory);

    public void AddDirectory(string name)
    {
        Current.Entries.Add(new DirectoryEntry(name, Current));
    }

    public void AddFile(string name, int size)
    {
        Current.Entries.Add(new FileEntry(name, size));
    }

    public void Print()
    {
        Root.Print(0);
    }
}

abstract record Entry(string Name)
{
    public abstract int TotalSize();
}
record FileEntry(string Name, int Size) : Entry(Name)
{
    public override string ToString() => $"{Name} {Size}";
    public override int TotalSize() => Size;
}

record DirectoryEntry(string Name, DirectoryEntry? Parent) : Entry(Name)
{
    public List<Entry> Entries {get;} = new();
    public void Print(int depth)
    {
        foreach (var entry in Entries)
        {
            Console.WriteLine("{0}{1}", new string(' ', depth * 2), entry);
            if (entry is DirectoryEntry subdir) {
                subdir.Print(depth + 1);
            }
        }
    }
    public override int TotalSize() => Entries.Sum(e => e.TotalSize());
    public override string ToString() => $"{Name} (dir)";
}
