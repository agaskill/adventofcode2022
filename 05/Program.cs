﻿using System.Text.RegularExpressions;

var lines = File.ReadAllLines(args[0]);
var lineOffset = 0;
while (!char.IsAsciiDigit(lines[lineOffset][1]))
    lineOffset++;

var stacks = ParseStacks(lines[lineOffset], lines.Take(lineOffset).Reverse().ToList());

foreach (var stack in stacks) {
    Console.WriteLine(new string(stack.ToArray()));
}

foreach (var moveLine in lines.Skip(lineOffset + 2))
{
    DoMove(stacks, moveLine);
}

Console.WriteLine("Stack tops: {0}", new string(stacks.Select(s => s.Peek()).ToArray()));

void DoMove(List<Stack<char>> stacks, string moveLine)
{
    Console.WriteLine(moveLine);
    var match = Regex.Match(moveLine, @"^move (\d+) from (\d+) to (\d+)");
    if (!match.Success) throw new InvalidDataException("Unexpected move format");
    var count = int.Parse(match.Groups[1].Value);
    var from = int.Parse(match.Groups[2].Value) - 1;
    var to = int.Parse(match.Groups[3].Value) - 1;
    var tmpStack = new Stack<char>();
    while (count-- > 0) {
        //stacks[to].Push(stacks[from].Pop());
        tmpStack.Push(stacks[from].Pop());
    }
    while (tmpStack.Count > 0) {
        stacks[to].Push(tmpStack.Pop());
    }
}

List<Stack<char>> ParseStacks(string labelLine, List<string> stackLines)
{
    var stacks = new List<Stack<char>>();
    for (var c = 1; c < labelLine.Length; c += 4) {
        var stackNum = int.Parse(labelLine.Substring(c, 1));
        Console.WriteLine("Stack {0}", stackNum);
        stacks.Add(ParseStack(c, stackLines));
    }
    return stacks;
}

Stack<char> ParseStack(int c, List<string> stackLines)
{
    var stack = new Stack<char>();
    foreach (var line in stackLines)
    {
        if (line.Length > c && !char.IsWhiteSpace(line[c]))
            stack.Push(line[c]);
    }
    return stack;
}