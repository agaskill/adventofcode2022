﻿var totalScore = 0;
foreach (var line in File.ReadLines(args[0]))
{
    var opponent = line[0] - 'A';
    var action = line[2] - 'X';
    var you = (opponent + (action - 1)) % 3;
    if (you < 0) you += 3;

    totalScore += (1 + you);
    totalScore += action * 3;
    // if (you == opponent) {
    //     //draw
    //     totalScore += 3;
    // }
    // else if ((opponent + 1) % 3 == you)
    // {
    //     // win
    //     totalScore += 6;
    // }
    // // else loss, no points
}
Console.WriteLine("Total score: {0}", totalScore);